<html>
<head>
    <title>BASIC MVC</title>
</head>
<body>
    <h1>Basic MVC contact form</h1>
    <p>
        Features:<br>
        MVC design
        Application logic is implemented in the model
    </p>
<?php
    echo (isset($error) && ($error != '')) ? $error : '';
?>
    <hr>
    <form method="POST" action="http://gigfestmvc1.dev/contact/send">
        <label for="email">Email</label><input id="email" name="email" value="<?php echo $model['email'];?>"><br>
        <label for="name">Name</label><input id="name" name="name" value="<?php echo $model['name'];?>"><br>
        <label for="subject">Subject</label><input id="subject" name="subject" value="<?php echo $model['subject'];?>"><br>
        <label for="message">Message</label><input id="message" name="message" value="<?php echo $model['message'];?>"><br>
        <button type="submit" value="submit">Submit</button>
    </form>
    <hr>
</body>
</html>