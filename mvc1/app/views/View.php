<?php
/**
 * Class View
 */
class View {

    /**
     * @param string $templateName
     * @param array $data
     */
    function render($templateName, $data = [])
    {
        extract($data);

        include APP_DIR . 'views/templates/' . $templateName . '.php';
    }

}