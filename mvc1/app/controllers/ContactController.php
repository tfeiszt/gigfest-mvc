<?php
/**
 * Class ContactController
 */
class ContactController {

    /**
     * @var array
     */
    protected $data = [];
    /**
     * @var View
     */
    private $view;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->view = new View();
    }


    /**
     * Show an empty form
     */
    public function actionIndex()
    {
        /**
         * Load model
         */
        require_once APP_DIR . 'models/ContactModel.php';

        /**
         * Create model
         */
        $model = new ContactModel();

        /**
         * Delegate data to the view
         */
        $this->data = [
            'error' => '', //$_SESSION['message'],
            'model' => $model->attributes
        ];
        //unset($_SESSION['message']);

        /**
         * Load view
         */
        $this->view->render('TplContactForm', $this->data);
    }


    /**
     * Do application logic
     */
    public function actionSend()
    {
        /**
         * Only POST request accepted
         */
        if (count($_POST) > 0) {
            /**
             * Load model
             */
            require_once APP_DIR . 'models/ContactModel.php';

            /**
             * Create model
             */
            $model = new ContactModel();

            /**
             * Delegate data to model
             */
            $model->loadAttributes($_POST);

            /**
             * Do validation
             */
            if ($model->validateData()) {

                /**
                 * Do application logic here
                 */
                if ($model->saveMessage()) {

                    /**
                     * SUCCESS, success message into session
                     */
                    //$_SESSION['message'] = 'Email has been sent. Thanks.';
                } else {
                    /**
                     * FAIL, error message into session
                     */
                    //$_SESSION['message'] = $model->error;
                }

                /**
                 * REDIRECT - we must redirect here anyway
                 * if sending has success, user should not be able to post the same data again (refresh browser)
                 * if error has occurred, it likely happend due to valid but incorrect data so form must be cleared.
                 */
                header('Location: ' . 'http://gigfestmvc1.dev/contact/index');
                exit;

            } else {

                /**
                 * Fail due to validation - Show the form and the message again filled with last posted data
                 */

                /**
                 * Delegate data to the view
                 */
                $this->data = [
                    'error' => $model->error,
                    'model' => $model->attributes
                ];

                /**
                 * Load view
                 */
                $this->view->render('TplContactForm', $this->data);
            }

        } else {
            echo "403 Request is not allowed";
            die(403);
        }
    }



}