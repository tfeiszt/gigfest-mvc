<?php
/**
 * Class ContactModel
 */
class ContactModel {

    /**
     * @var string
     */
    public $error = '';

    /**
     * @var array
     */
    public $attributes = [
        'name' => '',
        'email'=> '',
        'subject' => '',
        'message' => ''
    ];


    /**
     * @param array $attrs
     * @return bool
     */
    public function loadAttributes($attrs = [])
    {
        /**
         * load delegated data
         */
        foreach($this->attributes as $key => $item){
            if (isset($attrs[$key])){
                $this->attributes[$key] = $attrs[$key];
            }
        }
        return $this;
    }


    /**
     * @return bool
     */
    public function validateData()
    {
        /**
         * xss sanitize should be here
         */

        /**
         * Data validation
         */
        foreach($this->attributes as $key => $item){
            if ($item == '' ){
                $this->error .= ucwords($key) . ' field is required<br>';
            }
        }
        return ($this->error == '') ? true : false;
    }


    /**
     * @return $this|bool
     */
    public function saveMessage()
    {
        /**
         * Application logic:
         *
         * 1. Save data into Db
         * 2. Send email
         * 3. If everything OK, return $this
         * 4. Else return false
         *
         * So: Do an insert SQL or save it via ORM
         *
         * Then return mail('webmaster@somedomain.co.uk',
         *      $this->attributes['subject'],
         *      $this->attributes['message']);
         */
        if (1 == 1) {
            return $this;
        } else {
            return false;
        }
    }


}