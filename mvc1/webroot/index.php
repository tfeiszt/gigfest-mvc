<?php
/**
 * Bootstrap
 */

/**
 * set application environment
 */
error_reporting(E_ALL - E_STRICT);
@ini_set('display_errors', E_ALL - E_STRICT);
define('WEB_DIR' , './');
define('APP_DIR', '../app/');

/**
 * some basic configuration
 */
$config = [
    'default_controller' => 'contact',
    'default_action' => 'index'
];

/**
 * get URL
 */
$url = (isset($_SERVER['REQUEST_URI'])) ? $_SERVER['REQUEST_URI'] : '';
$urlArray = array();
$urlArray = explode("/",$url);
if (isset($urlArray[0])) {
    array_shift($urlArray);
}

/**
 * get controller parameter
 */
$controller = ((isset($urlArray[0])) && ($urlArray[0] != '')) ? $urlArray[0] : $config['default_controller'];

/**
 * get method parameter
 */
array_shift($urlArray);
$action = ((isset($urlArray[0])) && (trim($urlArray[0]) != '')) ? $urlArray[0] : $config['default_action'] ;

/**
 * rest of those are parameters
 */
array_shift($urlArray);
$parameters = $urlArray;


/**
 * get controller class name
 */
$className = ucwords($controller) . 'Controller';

/**
 * get method name
 */
$methodName = 'action' . ucwords($action);

/**
 * validate controller class
 */
if (file_exists(APP_DIR . 'controllers/' . $className .'.php')) {

    /**
     * Load other basic classes
     */
    require_once APP_DIR . 'views/View.php';

    /**
     * create a controller
     */
    require_once APP_DIR . 'controllers/' . $className .'.php';
    $dispatch = new $className();

    /**
     * validate controller instance
     */
    if ($dispatch) {

        /**
         * validate method
         */
        if (method_exists($dispatch, $methodName)) {

            /**
             * application logic entry point
             */
            call_user_func(array($dispatch, $methodName), []);

        } else {
            echo "404 Page not found";
            die(404);
        }
    } else {
        echo "500 An error occurred";
        die(500);
    }
} else {
    echo "404 Page not found";
    die(404);
}