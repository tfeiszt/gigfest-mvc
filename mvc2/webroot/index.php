<?php
/**
 * Bootstrap
 */

/**
 * set application environment
 */
error_reporting(E_ALL - E_STRICT);
@ini_set('display_errors', E_ALL - E_STRICT);
define('WEB_DIR' , './');
define('APP_DIR', '../app/');

/**
 * Some configuration
 */
$config = [
    'root_url' => 'http://gigfestmvc2.dev/',
    'default_controller' => 'contact',
    'default_action' => 'index'
];

/**
 * Load application class
 */
require_once APP_DIR . 'App.php';

/**
 * Create a singleton FRONT-CONTROLLER -> load configuration -> call applications entry point
 */
App::getInstance()->loadConfig($config)->run();
