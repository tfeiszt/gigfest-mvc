<?php

/**
 * Class ContactService
 *
 * Design: FACTORY
 * http://www.phptherightway.com/pages/Design-Patterns.html
 */
class ContactService {

    /**
     * @var
     */
    private static $instance;

    /**
     * @return static
     */
    public static function getInstance()
    {
        if (!isset(self::$instance)) {
            self::$instance = new static;
        }
        return self::$instance;
    }

    /**
     * @return ContactModel
     * @throws Exception
     */
    public static function getContactModel()
    {
        /**
         * Load model
         */
        App::getInstance()->loadClass('contact', 'model');
        return new ContactModel();

    }


    /**
     * @param ContactModel $contactModel
     * @return bool
     * @throws Exception
     */
    public static function sendContactMessage(ContactModel $contactModel)
    {
        /**
         * Application logic here:
         *
         * 1. Save some data into Db
         * 2. Send email
         * 3. If everything OK, return true
         * 4. If there was an error throw an exception
         */

        if ($contactModel->saveMessage() && ContactService::sendContactEmail($contactModel)) {
            return true;
        } else {
            throw new Exception('An error occurred [Unknown]');
        }

    }


    /**
     * @param ContactModel $contactModel
     * @return bool
     * @throws Exception
     */
    public static function sendContactEmail(ContactModel $contactModel)
    {
        /**
         * Part of application logic: send email.
         * It callable without saving. E.g: re-send an existing message.
         *
         * return mail('webmaster@somedomain.co.uk',
         *          $contactModel->attributes['subject'],
         *          $contactModel->attributes['message']);
         */
        if (1 == 1) {
            return true;
        } else {
            throw new Exception('An error occurred [Mailer error]');
        }
    }

}