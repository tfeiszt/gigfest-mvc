<?php

/**
 * Class ContactModel
 *
 * Design: STRATEGY
 * http://www.phptherightway.com/pages/Design-Patterns.html
 */
class ContactModel {

    /**
     * @var string
     */
    public $error = '';

    /**
     * @var array
     */
    public $attributes = [
        'name' => '',
        'email'=> '',
        'subject' => '',
        'message' => ''
    ];


    /**
     * @param array $attrs
     * @return bool
     */
    public function loadAttributes($attrs = [])
    {
        /**
         * load delegated data
         */
        foreach($this->attributes as $key => $item){
            if (isset($attrs[$key])){
                $this->attributes[$key] = $attrs[$key];
            }
        }
        return $this;
    }

    /**
     * @return bool
     */
    public function validateData()
    {
        /**
         * xss sanitize should be here
         */

        /**
         * Data validation
         */
        foreach($this->attributes as $key => $item){
            if ($item == '' ){
                $this->error .= ' ' . ucwords($key) . ' field is required<br>';
            }
        }
        return ($this->error == '') ? true : false;
    }

    /**
     * @return $this
     * @throws Exception
     */
    public function saveMessage(){
        /**
         * No application logic here
         *
         * Do an insert or update SQL or save it via ORM
         */
        if (1 == 1) {
            return $this;
        } else {
            throw new Exception('An error occurred [DB error]');
        }
    }


}