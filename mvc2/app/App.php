<?php

/**
 * Class App
 *
 * Design: SINGLETON
 * http://www.phptherightway.com/pages/Design-Patterns.html
 */
class App {

    /**
     * @var
     */
    private static $instance;

    /**
     * @var array
     */
    private $config = [];

    /**
     * @var array
     */
    private $urlArray = [];

    /**
     *
     */
    public function __construct()
    {
        $url = (isset($_SERVER['REQUEST_URI'])) ? $_SERVER['REQUEST_URI'] : '';
        $this->urlArray = array();
        $this->urlArray = explode("/",$url);
        if (isset($this->urlArray[0])) {
            array_shift($this->urlArray);
        }
    }

    /**
     * @return static
     */
    public static function getInstance()
    {
        if (!isset(self::$instance)) {
            self::$instance = new static;
        }
        return self::$instance;
    }

    /**
     * @param array $config
     * @return $this
     */
    public function loadConfig($config = [])
    {
        $this->config = $config;
        return $this;
    }


    /**
     * @param $key
     * @return array|null
     */
    public function getConfig($key)
    {
        if ($key == '') {
            return $this->config;
        } else {
            return (isset($this->config[$key])) ? $this->config[$key] : null;
        }
    }


    /**
     * @param int $element
     * @param string $default
     * @return string
     */
    public function getUri($element, $default)
    {
        return  ((isset($this->urlArray[$element])) && ($this->urlArray[$element] != '')) ? $this->urlArray[$element] : $default;
    }


    /**
     * @param string $name
     * @param string $type
     * @return bool
     * @throws Exception
     */
    public function loadClass($name, $type)
    {
        $fileName = ucwords($name) . ucwords($type);
        switch($type) {
            case 'controller':
                $location = 'controllers/';
                break;
            case 'model':
                $location = 'models/';
                break;
            case 'view':
                $location = 'views/';
                break;
            case 'service':
                $location = 'services/';
                break;
            default:
                $location = '';
        }
        if (file_exists(APP_DIR . $location . $fileName . '.php')) {
            require_once APP_DIR . $location . $fileName . '.php';

            return true;
        } else {
            throw new Exception('File not found: ' . APP_DIR . $location . $fileName . '.php');
        }
    }

    /**
     * @throws Exception
     */
    public function run()
    {
        /**
         * get controller parameter
         */
        $controller = $this->getUri(0, $this->getConfig('default_controller'));


        if ($this->loadClass($controller, 'controller')) {

            /**
             * Load other requirements
             */
            App::getInstance()->loadClass('default', 'view');

            /**
             * get controller class name
             */
            $className = ucwords($controller) . 'Controller';

            /**
             * Create controller instance
             */
            $dispatch = new $className();

            /**
             * validate action
             */
            if ($dispatch) {

                /**
                 * get method parameter
                 */
                $action = $this->getUri(1, $this->getConfig('default_action'));

                /**
                 * get method name
                 */
                $methodName = 'action' . ucwords($action);

                /**
                 * validate method
                 */
                if (method_exists($dispatch, $methodName)) {

                    /**
                     * application logic entry point
                     */
                    call_user_func(array($dispatch, $methodName), []);

                } else {
                    echo "404 Page not found";
                    die(404);
                }
            } else {
                echo "500 An error occurred";
                die(500);
            }
        } else {
            echo "404 Page not found";
            die(404);
        }
    }



}