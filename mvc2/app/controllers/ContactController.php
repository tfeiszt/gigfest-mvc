<?php

/**
 * Class ContactController
 *
 * Design: STRATEGY
 * http://www.phptherightway.com/pages/Design-Patterns.html
 */
class ContactController {

    /**
     * @var array
     */
    protected $data = [];
    /**
     * @var View
     */
    private $view;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->view = new View();
        /**
         * Load controller specified requirements - it depends on application logic
         */
        App::getInstance()->loadClass('contact', 'service');
    }


    /**
     * Show an empty form
     */
    public function actionIndex()
    {
        /**
         * Create model via factory
         */
        $model = ContactService::getInstance()->getContactModel();

        /**
         * Delegate data to the view
         */
        $this->data = [
            'error' => '', //$_SESSION['message'],
            'model' => $model->attributes
        ];
        //unset($_SESSION['message']);

        /**
         * Load view
         */
        $this->view->render('TplContactForm', $this->data);
    }


    /**
     * Do application logic
     */
    public function actionSend()
    {
        /**
         * Only POST request accepted
         */
        if (count($_POST) > 0) {
            /**
             * Create model via factory
             */
            $model = ContactService::getInstance()->getContactModel();

            /**
             * Delegate data to model
             */
            $model->loadAttributes($_POST);

            /**
             * Do validation
             */
            if ($model->validateData()) {

                /**
                 * Validation OK, do application logic
                 */
                try {
                    ContactService::getInstance()->sendContactMessage($model);
                    /**
                     * SUCCESS, success message into session
                     */
                    //$_SESSION['message'] = 'Email has been sent. Thanks.';
                } catch(Exception $e) {
                    /**
                     * FAIL, error message into session
                     */
                    //var_dump($e->getMessage()); exit;
                    //$_SESSION['message'] = getErrorMessageFromExceptionHandlerService($e);
                } finally {

                    /**
                     * REDIRECT - we must redirect here anyway
                     * if sending has success, user should not be able to post the same data again (refresh browser)
                     * if error has occurred, it likely happend due to valid but incorrect data so form must be cleared.
                     */
                    header('Location: ' . App::getInstance()->getConfig('root_url') . 'contact/index');
                    exit;
                }

            } else {

                /**
                 * Fail due to validation - Show the form and the message again filled with last posted data
                 */

                /**
                 * Delegate data to the view
                 */
                $this->data = [
                    'error' => $model->error,
                    'model' => $model->attributes
                ];

                /**
                 * Load view
                 */
                $this->view->render('TplContactForm', $this->data);
            }

        } else {
            echo "403 Request is not allowed";
            die(403);
        }
    }



}